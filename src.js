console.log('*****  Sample API Testing script using Newman and Postman *****');

var newman = require('newman');
var fs = require('fs');
var col =  './collections/';
var globVars = './global_variables/My Workspace.postman_globals';
var testData = './test_data/customers.csv';

runNewman();

function runNewman(){

        fs.readdir(col, function (err, files) {
            if (err) { throw err; }

            files = files.filter(function (file) {
                return (/^((?!(package(-lock)?))|.+)\.json/).test(file);
            });

            files.forEach(function (file) {
                console.log(file);
                newman.run({
                    collection: col+file,
                    global: globVars,
                    iterationData: testData,
                    reporters: ['cli', 'html']
                }).on('done', function (err, summary) {
                    if(err){
                        console.log("E R R O R >>", err);
                    }
                    console.info("COMPLETED - "+file);
            
                })
            })
         });
 }