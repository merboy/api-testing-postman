This project uses Newman and Postman to automate API testing. With Newman, you can run and test a Postman collection. It is built with extensibility in mind so that you can easily integrate it with your continuous integration servers and build systems.

TO RUN: NPM TEST